package model.critical_section;

import java.time.LocalDateTime;

public class Counter {
    private int number = 10;
    private static Object myObj = new Object();
    private static Object myObj1 = new Object();
    private static Object myObj2 = new Object();


    public void increment() {
        synchronized (myObj) {
            System.out.println(LocalDateTime.now());
            for (int i = 0; i < 1000000000; i++) {
                number++;
            }
            System.out.println(number);
        }
    }

    public void decrement() {
        synchronized (myObj1) {
            //synchronized (myObj) {

            System.out.println(LocalDateTime.now());
            for (int i = 0; i < 1000000000; i++) {
                number--;
            }
            System.out.println(number);
        }
    }

    public void sqrt() {
        synchronized (myObj2) {
            //synchronized (myObj) {
            System.out.println(LocalDateTime.now());
            for (int i = 0; i < 1000000000; i++) {
                number *= number;
            }
            System.out.println(number);

        }
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Number = " + number;
    }

    public void startThread() {
        Thread thread1 = new Thread(this::decrement);
        Thread thread2 = new Thread(this::increment);
        Thread thread3 = new Thread(this::sqrt);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
