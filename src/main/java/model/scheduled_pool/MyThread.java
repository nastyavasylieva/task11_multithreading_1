package model.scheduled_pool;

import java.util.Date;
import java.util.Random;

public class MyThread implements Runnable {
    private long sleepTime;

    public MyThread() {
        sleepTime = new Random().nextInt(5) * 1000;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Start. Time = " + new Date());
        processCommand();
        System.out.println(Thread.currentThread().getName() + " End. Time = " + new Date());
        System.out.println(Thread.currentThread().getName() + " Sleep time = " + sleepTime);

    }

    private void processCommand() {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
