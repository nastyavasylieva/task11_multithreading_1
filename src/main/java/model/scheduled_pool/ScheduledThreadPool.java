package model.scheduled_pool;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPool {
    public void go(int count) {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);

        System.out.println("Current Time = " + new Date());
        for (int i = 0; i < count; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MyThread thread = new MyThread();
            scheduledThreadPool.schedule(thread, 10, TimeUnit.SECONDS);
        }

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        scheduledThreadPool.shutdown();
        while (!scheduledThreadPool.isTerminated()) {
        }
        System.out.println("Finished all threads");
    }
}
