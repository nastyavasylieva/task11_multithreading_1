package model.fibonacci_sequence;

import java.util.concurrent.*;

public class FibonacciWithSum implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        return printFibonacciNumbers();
    }

    private int number;

    public FibonacciWithSum(int number) {
        this.number = number;
    }

    public int printFibonacciNumbers() {
        int sum = 1;
        int number1 = 0;
        int number2 = 1;
        System.out.println(number1);
        System.out.println(number2);

        for (int i = 0; i < number - 2; i++) {
            int number3 = number1 + number2;
            sum += number3;
            System.out.println(number3);
            number1 = number2;
            number2 = number3;
        }
        Thread thread = new Thread(() -> {

        });
        thread.start();
        return sum;
    }

    public int startThread() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> future = executorService.submit(new FibonacciWithSum(number));
        int result = 0;
        try {
            result = future.get().intValue();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

}
