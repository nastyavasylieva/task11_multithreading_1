package model.fibonacci_sequence;

public class SimpleFibonacci {
    private int number;

    public SimpleFibonacci(int number) {
        this.number = number;
    }

    public void printFibonacciNumbers() {
        int number1 = 0;
        int number2 = 1;
        System.out.println(number1);
        System.out.println(number2);

        for (int i = 0; i < number - 2; i++) {
            int number3 = number1 + number2;
            System.out.println(number3);
            number1 = number2;
            number2 = number3;
        }
        Thread thread = new Thread(() -> {

        });
        thread.start();
    }

    public void startThread() {
        Thread thread = new Thread(this::printFibonacciNumbers);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
