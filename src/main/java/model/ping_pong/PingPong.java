package model.ping_pong;

public class PingPong {
    private static Object sync = new Object();

    public void play() {
        Thread ping = new Thread(() -> {
            synchronized (sync) {
                System.out.println(Thread.currentThread().getName() + " started.");
                for (int i = 0; i < 10; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Ping!");
                    sync.notify();
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        });
        Thread pong = new Thread(() -> {
            synchronized (sync) {
                System.out.println(Thread.currentThread().getName() + " started.");
                for (int i = 0; i < 10; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Pong!");
                }
                System.out.println(Thread.currentThread().getName() + " finished.");
            }
        });

        ping.start();
        pong.start();
        try {
            ping.join();
            pong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
