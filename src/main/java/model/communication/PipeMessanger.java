package model.communication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;

public class PipeMessanger {

    private static Logger logger = LogManager.getLogger(PipeMessanger.class);

    final PipedOutputStream output;
    final PipedInputStream input;
    private static Object sync = new Object();

    public PipeMessanger() throws IOException {
        output = new PipedOutputStream();
        input = new PipedInputStream(output);
    }

    public void write() {
        try {
            synchronized (sync) {
                System.out.println("Write a message for Thread 2");
                Scanner scanner = new Scanner(System.in);
                String msg = scanner.nextLine();
                output.write(msg.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void read() {
        try {
            synchronized (sync) {
                int data = input.read();
                System.out.println("Hi! I received your message! It was...:");
                while (data != -1) {
                    System.out.print((char) data);
                    data = input.read();
                }
            }

        } catch (IOException e) {
        }
    }

    public void startCommunication() {
        Thread thread1 = new Thread(this::write);
        Thread thread2 = new Thread(this::read);
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("\n");
    }

}
