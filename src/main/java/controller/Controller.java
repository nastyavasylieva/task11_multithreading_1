package controller;

import model.communication.PipeMessanger;
import model.critical_section.Counter;
import model.fibonacci_sequence.FibonacciWithSum;
import model.fibonacci_sequence.SimpleFibonacci;
import model.ping_pong.PingPong;
import model.scheduled_pool.ScheduledThreadPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.ConsoleView;

import java.io.IOException;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void execute(final ConsoleView view, final int commandKey) throws IllegalArgumentException {
        switch (commandKey) {
            case 1:
                PingPong pingPong = new PingPong();
                pingPong.play();
                break;

            case 2:
                SimpleFibonacci task = new SimpleFibonacci(10);
                task.startThread();
                task.startThread();
                break;

            case 3:
                FibonacciWithSum task2 = new FibonacciWithSum(10);
                System.out.println("Sum of fibonacci elements: " + task2.startThread());
                FibonacciWithSum task3 = new FibonacciWithSum(6);
                System.out.println("Sum of fibonacci elements: " + task3.startThread());
                break;

            case 4:
                ScheduledThreadPool pool = new ScheduledThreadPool();
                pool.go(view.enterCountOfThread());
                break;

            case 5:
                Counter counter = new Counter();
                counter.startThread();
                break;

            case 6:
                PipeMessanger pipeMessanger = null;
                try {
                    pipeMessanger = new PipeMessanger();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert pipeMessanger != null;
                pipeMessanger.startCommunication();
                break;
            case 0:
                logger.info("Goodbye!");
                break;

            default:
                logger.info("Something was wrong!");

        }
    }

}
