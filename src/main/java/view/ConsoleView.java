package view;

import controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {
    private Map<Integer, String> menu;
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);


    public ConsoleView(Controller controller) {
        this.controller = controller;
        menu = new LinkedHashMap<>();
        menu.put(1, "1 - Start ping-pong game");
        menu.put(2, "2 - Print Fibonacci sequence");
        menu.put(3, "3 - Print Fibonacci sequence using Callable");
        menu.put(4, "4 - Start scheduled thread pool");
        menu.put(5, "5 - Start task with critical section");
        menu.put(6, "6 - Start communication between tasks");

        menu.put(0, "0 - Exit");
    }

    private void outputMenu() {
        logger.info("\nMENU:\n");
        for (Integer key : menu.keySet()) {
            logger.info(menu.get(key) + "\n");
        }
    }

    public void show() {
        int commandKey = 0;
        do {
            outputMenu();
            logger.info("Please, select menu item:\n");
            try {
                commandKey = input.nextInt();
                controller.execute(this, commandKey);
            } catch (InputMismatchException | IllegalArgumentException e) {
                logger.info("Incorrect data. Try again.");
                input.nextLine();
                continue;
            }
        } while (commandKey != 0);
    }

    public int enterCountOfThread() {
        logger.info("Please, enter number of threads.");
        try {
            return input.nextInt();
        } catch (InputMismatchException | IllegalArgumentException e) {

        }
        return 1;
    }
}
