import controller.Controller;
import view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        Controller controller = new Controller();
        ConsoleView consoleView = new ConsoleView(controller);
        consoleView.show();
    }
}
